package Game;

public class Angle implements Comparable<Angle> {
	public static final double EPSILON= 1E-6;
	//Technically, this should be the machine epsilon, but I don't know how to get that in Java.

	private double degrees;//we just store it as degrees.

	public Angle(){
	}

	public Angle(double degrees){
		this.degrees = degrees;
	}

	public void setDegrees(double degrees){
		this.degrees = degrees;
	}

	public void setRadians(double radians){
		this.degrees = Math.toDegrees(radians);
	}

	public double getDegrees(){
		return degrees;
	}

	public double getRadians(){
		return Math.toRadians(degrees);
	}

	public boolean equals(Object obj) {
		if(!(obj instanceof Angle)){
			return false;
		}
		return Math.abs(degrees-((Angle)obj).degrees)<=EPSILON;
	}

	@Override
	public int compareTo(Angle o) {
		if(o==null){
			return -1;//make sure we don't have nullpointer exception
		}
		return ((Double)degrees).compareTo(o.degrees);
	}
}
