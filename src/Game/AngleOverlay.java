package Game;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Set;
import java.util.TreeSet;

public class AngleOverlay {
	private Set<Angle> anglesToDraw;
	private boolean enabled;
	private static boolean useRadians;
	private static final int LINELENGTH = 300;/// length of each angle's guideline

	public AngleOverlay() {
		anglesToDraw = new TreeSet<Angle>();
		anglesToDraw.add(new Angle(0));
		anglesToDraw.add(new Angle(30));
		anglesToDraw.add(new Angle(45));
		anglesToDraw.add(new Angle(60));
		anglesToDraw.add(new Angle(90));
		anglesToDraw.add(new Angle(120));
		anglesToDraw.add(new Angle(135));
		anglesToDraw.add(new Angle(150));
		anglesToDraw.add(new Angle(180));
		useRadians = false;// default to degrees
		enabled = true;
	}

	public void draw(Graphics g) {
		if (!enabled){
			return;// don't draw if not enabled.
		}
		
		g.setColor(Color.black);
		
		for (Angle a : anglesToDraw) {
			int x2 = AnglerGame.getInstance().getAngler().getX();
			int x1 = (int) (x2 + Math.cos(a.getRadians()) * LINELENGTH);
			int y2 = AnglerGame.getInstance().getAngler().getY();
			int y1 = (int) (y2 - Math.sin(a.getRadians()) * LINELENGTH);
						
			g.drawLine(x1, y1, x2, y2);
			g.drawString(getLabel(a), x1, y1);
		}
	}

	public static String getLabel(Angle theta) {
		String label;
		
		if (useRadians){
			label = String.valueOf(theta.getRadians() / Math.PI);
		} else {
			label = String.valueOf(theta.getDegrees());
		}
		
		if (label.length() > 5){
			label = label.substring(0, 4);// only 5 sig figs.
		}
		
		label += useRadians ? "π" : "°";
		
		return label;
	}

	/**
	 * Turns on the AngleOverlay so it will be displayed.
	 */
	public void enable() {
		enabled = true;
		AnglerGame.getInstance().repaint();
	}

	/**
	 * Turns off the AngleOverlay so it will not be displayed.
	 */
	public void disable() {
		enabled = false;
		AnglerGame.getInstance().repaint();
	}

	public static void useDegrees() {
		useRadians = false;
	}

	public static void useRadians() {
		useRadians = true;
	}
}
