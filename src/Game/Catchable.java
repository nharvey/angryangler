package Game;

import java.awt.Graphics;

public interface Catchable {
	public void draw(Graphics g);//must be able to draw itself
	
	/**
	 * This function simulates a catch of the catchable object.  The object should
	 * draw itself in a different way after this function is called.
	 * @return the point value of the object caught, or 0 if there is none. 
	 */
	public int catchMe();

	public Angle getTheta();

	public double getR();
}
