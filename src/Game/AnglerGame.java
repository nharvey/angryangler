package Game;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


@SuppressWarnings("serial")
public class AnglerGame extends JFrame {

	private AngleOverlay overlay = new AngleOverlay();
	
	//possible multiplayer
	private List<Angler> anglers = new ArrayList<Angler>();
	private int cur;//index of current angler.
	
	private Set<Catchable> swimmers = new HashSet<Catchable>();

	private ControlPanel panel;
	private JMenuBar menubar;
	private JMenuItem radians;
	private JMenuItem degrees;
	private JCheckBox autoPower;
	private JCheckBox overlayOn;
	private JSlider powerSlider;
	
	/////////////////
	//CONSTANTS
	/////////////////
	//auto-correct the power by default?
	private final boolean defAutoPower = true;

	private static final double TOLERANCE = 5;
	public static final int WIDTH = 800;
	public static final int HEIGHT = 600;
	private static final int TOTAL_FISH = 30;
	
	/////////////////
	//END CONSTANTS
	/////////////////
	
	private static AnglerGame instance;

	private AnglerGame(String title){//private to defeat multiple instantiation
		super(title);
		
		setSize(WIDTH, HEIGHT);
		setLayout(new BorderLayout());
		
		//TODO: prompt and set number of players if going with multiplayer
		try {
			anglers.add(new Angler(400,500));
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			reset();//start a new game
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		overlay = new AngleOverlay();
		panel = new ControlPanel();
		menubar = new JMenuBar();
		
		JMenu anglemenu= new JMenu("Angle Measure");
		
		radians = new JMenuItem("Radians");
		degrees = new JMenuItem("Degrees");
		degrees.addActionListener(new ActionListener() {
			@Override public void actionPerformed(ActionEvent arg0) {
				AngleOverlay.useDegrees();
				panel.useDegrees();
				repaint();
			}
		});
		radians.addActionListener(new ActionListener() {
			@Override public void actionPerformed(ActionEvent arg0) {
				AngleOverlay.useRadians();
				panel.useRadians();
				repaint();
			}
		});
		
		anglemenu.add(radians);
		anglemenu.add(degrees);
		
		menubar.add(anglemenu);
		
		autoPower = new JCheckBox("Max Power!", defAutoPower);
		autoPower.setBorder(null);//don't know why it has a dumb-looking default border
		autoPower.addItemListener(new MenuItemListener());
		
		overlayOn = new JCheckBox("Show Angle Overlay",true);
		overlayOn.addItemListener(new MenuItemListener());
		
		menubar.add(autoPower);
		menubar.add(overlayOn);
		setJMenuBar(menubar);
		
		ImageIcon bkground = new ImageIcon("AngryAnglerBackground.png");
		Image img = bkground.getImage();
		Image newImg = img.getScaledInstance(WIDTH - 75, HEIGHT - 75, java.awt.Image.SCALE_SMOOTH);
		
		add(new JLabel(new ImageIcon(newImg)),BorderLayout.CENTER);
		
		powerSlider = new JSlider(JSlider.VERTICAL);
		powerSlider.setMaximum(WIDTH/2);
		powerSlider.setMajorTickSpacing(10);
		powerSlider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				getAngler().choosePower(powerSlider.getValue());
			}
		});
		
		panel = new ControlPanel();
		
		add(panel,BorderLayout.SOUTH);
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent arg0) {
			}
			@Override
			public void focusGained(FocusEvent arg0) {
				AnglerGame.this.repaint();
			}
		});
		
	}

	public static AnglerGame getInstance(){
		if(instance == null){
			instance = new AnglerGame("Angry Angler");
		}
		return instance;
	}

	/**
	 * @return The current angler.
	 */
	public Angler getAngler(){
		return anglers.get(cur);
	}

	@Override
	public void paint(Graphics g){
		super.paint(g);
		
		for(Catchable f : swimmers){
			f.draw(g);
		}
		
		overlay.draw(g);
		
		for(Angler a : anglers){
			a.draw(g);
		}
	}

	/**
	 * Adds a fish with the following parameters:

	 * @param r distance from a static origin 
	 * @see Angler.setX()
	 * @param theta angle, in degrees.
	 * @param pts point value for catching the fish
	 * @param color
	 */
	public void addFish(double r, double theta, int pts, Color color) {
		swimmers.add(new Fish(r,theta,pts,color));
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		AnglerGame game = getInstance();
		game.setVisible(true);
		game.setResizable(false);
	}

	public void catchAll() {
		for (Catchable f : swimmers){
			getAngler().addPoints(f.catchMe());
		}
		swimmers.clear();
	}

	public Set<Catchable> getSwimmers() {
		return swimmers;
	}

	public boolean catchFishAt(double r, Angle theta) {
		Catchable prize = new Fish(Double.MAX_VALUE, 0, 0, null);
		boolean returnable = false;
		
		for(Catchable f : swimmers){
			double thetaLocation = Math.abs(f.getTheta().getDegrees() - theta.getDegrees());
			double rLocation = Math.abs(f.getR() - r);
			
			if(thetaLocation < TOLERANCE && rLocation < TOLERANCE){
				prize = f;
				returnable = true;
				break;//don't catch multiple
			}
		}
		
		getAngler().addPoints(prize.catchMe());
		
		swimmers.remove(prize);
		
		if(swimmers.size() == 0) {
			gameWin();
		}
		
		return returnable;
	}
	
	public void gameWin() {
		JOptionPane pane = new JOptionPane();
		pane.setSize(100, 100);
		
		//TODO: give option of playing again or exiting
		JOptionPane.showMessageDialog(pane, "Congratulations!\nClick OK to play again", "YOU WIN!", 1);
				
		try {
			AnglerGame.getInstance().reset();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void reset() throws IOException {
		swimmers.clear();
		
		for(int i=0; i<TOTAL_FISH; i++){
			swimmers.add(new Fish());
		}
				
		anglers.clear();
		anglers.add(new Angler(400,500));

		cur = 0;//Start at the first player
		
		repaint();
	}
	
	class MenuItemListener implements ItemListener{
		@Override
		public void itemStateChanged(ItemEvent e) {
			Object box = e.getSource();
			if(e.getStateChange()==ItemEvent.SELECTED){
				if(box==autoPower){
					getAngler().choosePower(Angler.AUTOCORRECT);
					remove(powerSlider);
					validate();
					repaint();
				}else if(box==overlayOn){
					overlay.enable();
				}
			}else //Deselected
				if(box==autoPower){
					add(powerSlider,BorderLayout.WEST);
					getAngler().choosePower(powerSlider.getValue());
					validate();
					repaint();
				}else if(box==overlayOn) overlay.disable();
		}
	}

}
