package Game;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Timer;

public class Angler {
	
	private Angle chosenAngle;
	private double chosenPower;
	private Timer time;
	private int points;
	private String name;
	private int x = 80;
	private int y = 80;
	private double r = 0;
	
	public static final double AUTOCORRECT = AnglerGame.WIDTH/2.1;
	private static final Image angler = Toolkit.getDefaultToolkit().createImage("angler.png");
	private static Image harpoon;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPoints() {
		return points;
	}

	public void addPoints(int score) {
		this.points += score;
		ControlPanel.setPointsText(String.valueOf(points));
	}
	
	public void clearPoints() {
		this.points = 0;
		ControlPanel.setPointsText("0");
	}

	public Angler() throws IOException {
		points = 0;
		chosenAngle = new Angle(0);
		chosenPower = AUTOCORRECT;//arbitrary value to signal auto-correction
		harpoon = ImageIO.read(new File("harpoon.png"));
	}

	public Angler(int x, int y) throws IOException {
		this.x = x;
		this.y = y;
		points = 0;
		chosenAngle = new Angle(0);
		chosenPower = AUTOCORRECT;//arbitrary value to signal auto-correction
		harpoon = ImageIO.read(new File("harpoon.png"));
	}

	public Angle getChosenAngle() {
		return chosenAngle;
	}
	
	public void chooseAngle(Angle chosenAngle) {
		this.chosenAngle = chosenAngle;
	}
	
	public double getChosenPower() {
		return chosenPower;
	}
	
	public void choosePower(double chosenPower) {
		this.chosenPower = chosenPower;
	}

	public void draw(Graphics g){
		int xd = (int) (x+r*Math.cos(chosenAngle.getRadians()));
		int yd = (int) (y-r*Math.sin(chosenAngle.getRadians())-5);
		
		AffineTransform tx = AffineTransform.getTranslateInstance(xd, yd);
		tx.concatenate(AffineTransform.getRotateInstance(-chosenAngle.getRadians(), 0, 5));
		
		AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
		
		BufferedImage temp = new BufferedImage(800,800,((BufferedImage)harpoon).getType());
		op.filter((BufferedImage) harpoon, temp);
		
		g.drawImage(temp,(int) 0, 0,null);
		g.drawImage(angler, x - (angler.getWidth(null)/2), y + 5, null);
		g.drawImage(op.filter((BufferedImage) harpoon, null),(int) 0, 0,null);
	}

	public void castLine(Graphics g) {
		draw(AnglerGame.getInstance().getGraphics());
		time = new Timer(50, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(r >= chosenPower ||	AnglerGame.getInstance().catchFishAt(r+50,chosenAngle)){
					((Timer) e.getSource()).stop();
					r=0;
					AnglerGame.getInstance().repaint();
				}else {
					r+=10;
					AnglerGame.getInstance().repaint();
				}
			}
		});
		time.start();
	}

	public int getX() {
		return x;
	}
	
	/**
	 * Sets the x-coordinate of the origin to which most game elements are related.
	 * @param x
	 */
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	
	/**
	 * Sets the y-coordinate of the origin to which most game elements are related.
	 * @param y
	 */
	public void setY(int y) {
		this.y = y;
	}
}
