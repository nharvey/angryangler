package Game;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;

public class Fish implements Catchable {

	private Angle theta;
	private double r;
	private int value;
	private Color color;
	private boolean caught;
	
	public static final int WIDTH = 20;
	public static final int HEIGHT = 10;
	public static final int MAXPOINTS = 20;
	
	/**
	 * Makes a new fish.
	 * @param r  distance from origin
	 * @param theta  angle from horizontal, in degrees.
	 * @param pts  point value of the fish.
	 * @param color  color with which to draw the fish.
	 */
	public Fish(double r, double theta, int pts, Color color) {
		this.r = r;
		this.theta = new Angle(theta);
		this.color=color;
		this.value = pts;
	}
	
	public Fish(){
		this.r = Math.random()*AnglerGame.WIDTH/2.1;
		
		while(r<60){
			r*=2;
		}
		
		this.theta = new Angle(Math.random()*180);
		this.color = new Color((float)Math.random(), (float)Math.random(), (float)Math.random());
		this.value = (int) (Math.random()*MAXPOINTS);
		
		if(value<=0){
			value = 1; //Make sure at least worth 1 point.
		}
	}
	
	public void draw(Graphics g){
		if(caught){
			return;
		}
		
		g.setColor(color);
		g.fillOval(getX(),getY(), WIDTH, HEIGHT);
		
		int[] xpoints = {getX()+WIDTH,getX()+WIDTH+WIDTH/4,getX()+WIDTH+WIDTH/4};
		int[] ypoints = {getY()+HEIGHT/2,getY(),getY()+HEIGHT};
		
		Polygon p = new Polygon(xpoints, ypoints, 3);
		g.fillPolygon(p);
	}

	/**
	 * @Deprecated use Angler.setX(int x) instead
	 * @param i
	 */
	@Deprecated
	public static void setOriginX(int i) {
		AnglerGame.getInstance().getAngler().setX(i);
	}
	
	/**
	 * @Deprecated use Angler.setY(int y) instead
	 * @param i
	 */
	@Deprecated
	public static void setOriginY(int i) {
		AnglerGame.getInstance().getAngler().setY(i);
	}

	/**
	 * @return the x coordinate for drawing the fish
	 */
	public int getX() {
		return (int) (AnglerGame.getInstance().getAngler().getX()+(r*Math.cos(theta.getRadians()))) - WIDTH/2;
	}
	/**
	 * @return the y coordinate for drawing the fish
	 */
	public int getY() {
		return (int) (AnglerGame.getInstance().getAngler().getY()-(r*Math.sin(theta.getRadians()))) - HEIGHT/2;
	}

	public Angle getTheta() {
		return theta;
	}

	public double getR() {
		return r;
	}
	
	public void setR(double r) {
		this.r = r;
	}

	public int getValue() {
		return value;
	}

	@Override
	public int catchMe() {
		caught = true;
		return value;
	}

}
