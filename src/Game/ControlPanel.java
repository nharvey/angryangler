package Game;

import java.awt.Button;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Hashtable;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

@SuppressWarnings("serial")
public class ControlPanel extends JPanel {
	private static JTextField points;
	private JSlider angleChooser; 
	private Button castButton = new Button("Fire!");
	
	private Hashtable<Integer, JLabel> degreeLabelTable = new Hashtable<Integer, JLabel>();
	private Hashtable<Integer, JLabel> radianLabelTable = new Hashtable<Integer, JLabel>();
	
	public ControlPanel() {
		super();
		setSize(100, 400);
		
		points = new JTextField("0");
		points.setColumns(7);
		points.setEditable(false);
		points.setBorder(new TitledBorder("Points"));
		add(points);
		
		for(int i = 0; i < 181; i = i + 10) {
			degreeLabelTable.put(new Integer(i), new JLabel(Integer.toString(i) + "\u00B0"));
		}
		
		AngleOverlay.useRadians();
		
		for(int i = 0; i < 181; i = i + 20) {
			radianLabelTable.put(new Integer(i), new JLabel(AngleOverlay.getLabel(new Angle(i))));
		}
		
		AngleOverlay.useDegrees();
		
		angleChooser = new JSlider();
		angleChooser.setInverted(true);
		angleChooser.setLabelTable(degreeLabelTable);
		angleChooser.setPreferredSize(new Dimension(560,50));
		angleChooser.setValue(0);
		angleChooser.setMajorTickSpacing(10);
		angleChooser.setMinorTickSpacing(5);
		angleChooser.setPaintTicks(true);
		angleChooser.setPaintLabels(true);
		angleChooser.setMaximum(180);
		angleChooser.setMinimum(0);
		
		angleChooser.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				AnglerGame.getInstance().getAngler().chooseAngle(new Angle(angleChooser.getValue()));
				AnglerGame.getInstance().repaint();
			}
		});
		
		angleChooser.setPaintLabels(true);
		add(angleChooser);
		
		castButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
//				AnglerGame.getInstance().getAngler().chooseAngle(new Angle(angleChooser.getValue()));
				AnglerGame.getInstance().getAngler().castLine(AnglerGame.getInstance().getGraphics());
			}
		});
		add(castButton);
	}

	public void useDegrees(){
		angleChooser.setLabelTable(degreeLabelTable);
	}
	
	public void useRadians(){
		angleChooser.setLabelTable(radianLabelTable);
	}
	
	/**
	 * Sets the points TextField to the passed string, if it is less than 7
	 * characters.
	 * 
	 * @param pts
	 * @return true if the points were successfully set.
	 */
	public static boolean setPointsText(String pts) {
		if (pts.length() < 7) {
			points.setText(pts);
			return true;
		} else
			return false;
	}
}
