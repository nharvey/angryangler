package tests;

import java.awt.Color;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import Game.AnglerGame;
import Game.Catchable;
import Game.Fish;


public class FishTests {

	@Test
	public void testConversion() {
		AnglerGame game = AnglerGame.getInstance();
		Fish.setOriginX(0);
		Fish.setOriginY(0);
		Fish test1 = new Fish(80, 45, 5, Color.RED);
		Assert.assertEquals(56, test1.getX(),0.0002);
		Assert.assertEquals(-56, test1.getY(),0.0002);
		
		Fish test2 = new Fish(160, 60, 5, Color.RED);
		Assert.assertEquals(80, test2.getX(),0.0002);
		Assert.assertEquals(-138, test2.getY(),0.0002);
		
		Fish test3 = new Fish(80, 60, 5, Color.RED);
		Assert.assertEquals(40, test3.getX(),0.0002);
		Assert.assertEquals(-69, test3.getY(),0.0002);
		
		Fish test4 = new Fish(4, 0, 5, Color.RED);
		Assert.assertEquals(4, test4.getX(),0.0002);
		Assert.assertEquals(0, test4.getY(),0.0002);
		
		Fish test5 = new Fish(0, 0, 5, Color.RED);
		Assert.assertEquals(0.00, test5.getX(),0.0002);
		Assert.assertEquals(0.00, test5.getY(),0.0002);
		
		Fish test6 = new Fish(3, 180, 5, Color.RED);
		Assert.assertEquals(-3.00, test6.getX(),0.0002);
		Assert.assertEquals(0.00, test6.getY(),0.0002);
		
		Fish.setOriginX(5);
		Fish.setOriginY(5);
		Assert.assertEquals(5.00, test5.getX(),0.0002);
		Assert.assertEquals(5.00, test5.getY(),0.0002);
	}
	
	@Test
	public void testDistribution() {
		AnglerGame game = AnglerGame.getInstance();
		Set<Catchable> fishGroup = game.getSwimmers();
		Set<Catchable> fishCopy = new HashSet<Catchable>(fishGroup);
		for(int i = 0; i < 5; ++ i) {
			Random rand = new Random();
			int random = Math.abs(rand.nextInt());
			int random2 = Math.abs(rand.nextInt());
			fishGroup.add(new Fish(random % 540, random2 % 180, 5, Color.RED ));
		}
		
		for(int i = 0; i < fishGroup.size(); ++ i) {
			fishCopy = fishGroup;
			fishCopy.remove(i);
			Assert.assertFalse(fishCopy.contains(i));
		}
	}

}
