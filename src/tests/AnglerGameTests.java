package tests;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;

import Game.Angler;
import Game.AnglerGame;
import Game.Fish;

public class AnglerGameTests {
	
	static AnglerGame game;
	static Angler angler;
	
	@BeforeClass
	public static void setUp() throws IOException{
		game = AnglerGame.getInstance();
		game.reset();
		
		angler = game.getAngler();
	}
	
	/*
	@Test
	public void initializationTest(){

	}
	*/
	
	@Test
	public void catchAllTests(){
		String pointsmsg = "points did not match expected";
		String clearMsg = "number of remaining fish differs from expected";
		
		int expectedPoints = 0;
		//ensure there are correct number of initial points
		assertEquals(pointsmsg, expectedPoints, angler.getPoints());
				
		game.addFish(1, 1, 1, null);
		game.addFish(2, 2, 2, null);
		game.addFish(3, 3, 3, null);
		expectedPoints = 6;
		
		// ensure anglers points increase when fish caught
		game.catchAll();
		assertEquals(pointsmsg, expectedPoints, angler.getPoints());
		
		//ensure fish are cleared after catchAll
		assertEquals(clearMsg, 0, game.getSwimmers().size());
		
		game.addFish(4, 4, 4, null);
		expectedPoints = 10;
		game.catchAll();
		assertEquals(pointsmsg, expectedPoints, angler.getPoints());
		
	}

}
