package tests;

//Author: Nathan Harvey
import static org.junit.Assert.assertEquals;

import java.awt.Color;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import Game.Angle;
import Game.Angler;
import Game.AnglerGame;

public class AnglerTests {
	AnglerGame game;
	Angler joe;
	@Before
	public void setUp() throws Exception {
		game = AnglerGame.getInstance();
		game.reset();
		joe = game.getAngler();
	}

	@Test
	public void testAngleSelection() {
		joe.chooseAngle(new Angle(45.0));
		joe.choosePower(4.0);
		assertEquals(45.0, joe.getChosenAngle().getDegrees(),0.0004);
		assertEquals(4.0, joe.getChosenPower(),0.0004);
		game.addFish(4.0,45.0,2,Color.red);
		game.addFish(3.0,45.0,5,Color.red);
		joe.castLine(game.getGraphics());
		assertEquals(2,joe.getPoints());//Caught the fish.
	}
	
	@Test
	public void testPowerCorrection() throws IOException {
		game.reset();
		assertEquals(0, joe.getPoints());
		joe = game.getAngler();
		joe.chooseAngle(new Angle(45.0));
		//joe.setChosenPower(4.0);
		//If we don't set power, correct automatically.
		game.addFish(4.0,45.0,2,Color.red);
		game.addFish(6.0,45.0,5,Color.red);
		assertEquals(2, game.getSwimmers().size());
		joe.castLine(game.getGraphics());
		assertEquals(2,joe.getPoints());//Caught the fish.
	}
}
