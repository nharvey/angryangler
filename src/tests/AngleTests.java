package tests;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import Game.Angle;
import Game.AnglerGame;

public class AngleTests {
	
	@Test
	public void testDegreestoRadians() {
		String msg = "returned radians did not match expected";
		Angle angle = new Angle(180.0);
		
		double expected = Math.PI;
		assertEquals(msg, expected, angle.getRadians(), 0);
		expected = Math.toRadians(27.0);
		angle.setDegrees(27.0);
		assertEquals(msg, expected, angle.getRadians(), 0);
		
	}
	
	@Test
	public void testRadianstoDegrees() {
		String msg = "returned degrees did not match expected";
		Angle angle = new Angle();
		angle.setRadians(Math.PI);
		
		double expected = Math.toDegrees(Math.PI);
		assertEquals(msg, expected, angle.getDegrees(), 0);
		
		expected = Math.toDegrees(27.0);
		angle.setRadians(27.0);
		assertEquals(msg, expected, angle.getDegrees(), 0);
		
	}
}
